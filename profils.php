<?php
	session_start();
	require 'icsparser/class.iCalReader.php';
	require 'config.php';
	require 'urls.php';
?>
<html>
	<head>
		<title>Profils</title>
		<meta charset="UTF-8">
		<link rel="stylesheet" type="text/css" href="style/style.css">
	</head>
<body>
	<header>
<?php
if(!($_SESSION['active'])) {
?>
	<p>Vous n'avez pas accès à cette ressource, veuillez vous connecter.</p>
	<p><a href="index.php"><input type="button" value="Connexion" /></a></p>
<?php
} else {
?>
<div>
<div>
    <form method="post" action="actions.php">
        <table>
            <tr>
                <td>Bonjour <?php echo $_SESSION['login']; ?> !</td>
                <td><input type="submit" value="Déconnexion" name="disconnect"/></td>
                <td><input type="submit" value="Rafraichir" name="refresh"></td>
                <td><input type="submit" value="Profils" name="profiles"/></td>
                <td><input type="submit" value="Agenda" name="agenda"/></td>
            </tr>
        </table>
    </form>
</div>
</div>
	</header>

<!-- On créé les tableaux HTML qui vont contenir les urls des agendas -->
	<table>
		<tr>
			<th>URL</th>
			<th>Couleur</th>
		</tr>

<?php

        foreach($_SESSION['urls'] as $key => $value) {
		echo '<tr>';
			echo '<td>'.$value['url'].'</td>';
			echo '<td style="background-color:'.$value['color'].'"></td>';
			echo '<td><a href="actions.php?line='.$key.'"><input type="button" value="Supprimer" name="'.$key.'"></a></td>';
			echo '</tr>';
		}
?>
		<form method="post" action="actions.php">
				<tr>
					<td><input type="text" name="url"></td>
					<td><input type="color" name="color"></td>
					<td><input type="submit" value ="Ajouter" name="addUrl"></td>
				</tr>

		</form>
	</table>
	<footer>
<?php
		var_dump($_SESSION['agendas']);
		echo '</br>';
		var_dump($_SESSION['urls']);
}
?>
	</footer>
</body>
