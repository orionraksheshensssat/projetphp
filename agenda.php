<?php
	session_start();
	require 'icsparser/class.iCalReader.php';
	require 'config.php';
?>
<html>
	<head>
		<title>Agenda(s)</title>
		<meta charset="UTF-8">
		<link rel="stylesheet" type="text/css" href="style/style.css">
	</head>
<body>
	<header>


<?php
if(!($_SESSION['active'])) {
?>
	<p>Vous n'avez pas accès à cette ressource, veuillez vous connecter.</p>
	<form method="post" action="actions.php">
        <input type="submit" value="Connexion" name="connect"/>
    </form>

<?php
} else {
?>
<div>
    <form method="post" action="actions.php">
        <table>
            <tr>
                <td>Bonjour <?php echo $_SESSION['login']; ?> !</td>
                <td><input type="submit" value="Déconnexion" name="disconnect"/></td>
                <td><input type="submit" value="Rafraichir" name="refresh"></td>
                <td><input type="submit" value="Profils" name="profiles"/></td>
                <td><input type="submit" value="Agenda" name="agenda"/></td>
            </tr>
        </table>
    </form>
</div>
	</header>
	<a href=""></a><input type="submit" value="<"/></a>
	<div class="agenda">
<?php
		$week = $_GET['week'];
		$a = $_SESSION['agendas'][0];

		$hourOffset = 30;
		$minOffset = $hourOffset/60;
		$dayOffset = 100;
		$margin = 5;
		$semaine = ["Lundi", "Mardi", "mercredi", "jeudi", "vendredi", "samedi", "dimanche"];


		if (isset($_GET['week'])) {

			//affichage des heures
			for($i = 7; $i < 21; $i++ ) {
				echo '<div class="agenda-block" style="top:'.($hourOffset+ $margin + ($hourOffset + $margin) * ($i - 7)).'px ;width:'.$dayOffset.'px; height:'.$hourOffset.'px">';
				echo ($i).'h';
				echo '</div>';
			}
			//affichage des jours de la semaine
			for ($i = 0; $i < 7; $i ++) {
				echo '<div class="agenda-block" style="left:'.($dayOffset + $margin + (($dayOffset + $margin) * $i)).'px; width:'.$dayOffset.'px; height:'.$hourOffset.'px"  >';
				echo $semaine[$i];
				echo '</div>';
			}
			$color = $_SESSION['urls'][0]['color'];
			foreach($a as $key => $value) {
				//echo $value['SUMMARY'];
				//on vérifie que les évènements affichés sont dans la bonne semaine
				if(date( "W",strtotime($value['DTSTART'])) == $week) {
					$jour		= date( "N", strtotime($value['DTSTART']));
					$hourStart 	= date( "G", strtotime($value['DTSTART']));
					$hourEnd 	= date( "G", strtotime($value['DTEND']));
					$minStart 	= date( "i", strtotime($value['DTSTART']));
					$minStart 	-= $minStart % 15;
					$minEnd 	= date( "i", strtotime($value['DTEND']));
					$minEnd		-= $minEnd % 15;

					$height 	= (($hourEnd*60 + $minEnd) - ($hourStart*60 + $minStart)) * $minOffset;
					$topEvt 	= ((1 + $hourStart - 7) * ($hourOffset + $margin) + $minStart * $minOffset);
					$leftEvt 	= $jour * ($dayOffset + $margin);


					echo '<div class="agenda-block" style="top:'.$topEvt.'px   ; left:'.$leftEvt.'px; width:'.$dayOffset.'px; height:'.$height.'px; background-color: '.$color.'"  >';
					echo $value['SUMMARY'].'<br>';
					echo '</div>';
				}
			}

			/*$ical = $_SESSION['ical'][0];
			echo $a[0]['SUMMARY'].'<br>';
			echo $a[0]['DTSTART'].'<br>';
			echo date( "W",strtotime($a[0]['DTSTART']));*/
		}
?>
	</div>
	<footer>
<?php
}
?>
	</footer>
</body>
