<?php
	session_start();
	require 'icsparser/class.iCalReader.php';
	require 'config.php';
?>
<html>
	<head>
		<title>ACCESS_DENIED</title>
		<meta charset="UTF-8">
		<link r	el="stylesheet" type="text/css" href="style/style.css">
	</head>
<body>

	<p>Vous n'avez pas accès à cette ressource, veuillez vous connecter.</p>
	<p><a href="index.php"> <input type="button" value="Connexion" /></a></p>
	<footer>
<?php
	include 'footer.php';
?>
	</footer>
</body>
