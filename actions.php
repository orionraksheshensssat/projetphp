<?php

session_start();
require 'urls.php';
require 'config.php';
require 'icsparser/class.iCalReader.php';
if(isset($_SESSION['login'])) {
	$path = 'urls/'.$_SESSION['login'].'.json';
}

$currentWeek = date("W", time());

function add_agenda($url, $path) {
	$ical = new ICal($url);
	$_SESSION['ical'][] = $ical;
	$_SESSION['agendas'][] = $ical->events();
	file_put_contents($path, json_encode($_SESSION['urls'], true));
}

function refresh($path) {
	unset($_SESSION['urls']);
	$file = file_get_contents($path);
	$_SESSION['urls'] = json_decode($file, true);
}

function delete_line($line) {
	unset($_SESSION['agendas'][$line]);
	unset($_SESSION['urls'][$line]);
	unset($_SESSION['ical'][$line]);
	file_put_contents($path, json_encode($_SESSION['urls'], true));
}


if(isset($_POST['disconnect'])) {
	///////////////////////////
	//Gestion de la déconnexion
    unset($_SESSION['login']);
    unset($_SESSION['password']);
	unset($_SESSION['urls']);
	unset($_SESSION['agendas']);
	unset($_SESSION['ical']);
    $_SESSION['active'] = false;
    header('Location: index.php');
    exit();
} elseif(isset($_POST['connect'])) {
	/////////////////////////
	//Gestion de la connexion
	if($_POST['login'] == $login && $_POST['password'] == $password) {
		$_SESSION['login'] = $_POST['login'];
		$_SESSION['password'] = $_POST['password'];
		$_SESSION['active'] = true;
		$path = 'urls/'.$_SESSION['login'].'.json';
		refresh($path);
		foreach($_SESSION['urls'] as $value) {
			add_agenda($value['url'], $path);
		}
		header('Location: agenda.php?week='.$currentWeek);
		exit();
	} else {
        header('Location: index.php');
        exit();
    }
} elseif(isset($_POST['profiles'])) {
	///////////////////////////
	//Clic sur le bouton profil
    header('Location: profils.php');
    exit();
} elseif(isset($_POST['agenda'])) {
	////////////////////////////
	//Clic sur le bouton agendas
    header('Location: agenda.php?week='.$currentWeek);
    exit();
} elseif(isset($_POST['addUrl'])) {
	//Gestion de l'jout d'une URL de calendrier iCal
	$a['url'] = $_POST['url'];
	$a['color'] = $_POST['color'];
   	$_SESSION['urls'][] = $a;
	file_put_contents($path, json_encode($_SESSION['urls'], true));
	add_agenda($a['url'], $path);
	refresh($path);
	header('Location: profils.php');
    exit();
} elseif(isset($_POST['refresh'])) {
	// Refaîchissement des données des agendas
	refresh($path);
	header('Location: profils.php');
    exit();
} elseif(isset($_GET['line'])) {
	//Gestion de la suppression d'une url
	delete_line($_GET['line']);
	header('Location: profils.php');
    exit();
}
?>
